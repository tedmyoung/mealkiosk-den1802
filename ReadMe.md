# Meal Kiosk

Starter project for Java class.

## Trying it out

To make sure this works on your machine, clone this repository and run the Maven build command, e.g.:

```
> mvn clean test
```

Should result in a bunch of files downloading, compiling the code, and then running the tests, which should pass.
If all goes well, you should see:

```
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  2.675 s
```
