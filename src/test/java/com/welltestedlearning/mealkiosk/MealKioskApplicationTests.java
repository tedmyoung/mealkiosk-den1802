package com.welltestedlearning.mealkiosk;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

// Commented out to disable this until we're ready to dig into Spring tests
//@RunWith(SpringRunner.class)
//@SpringBootTest
public class MealKioskApplicationTests {

	@Test
	public void contextLoads() {
	}

}
